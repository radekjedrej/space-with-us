class MobileNav {
    constructor() {
        this.menuBtn = $(".js-quilter-nav__btn")
        this.menu = $(".js-doctor-mobile-nav")
        this.mobileHasChild = $(".main-menu--mobile__ul .menu-item-has-children")
        this.mobileMenuExpand = $(".main-menu--mobile__ul .menu-item-has-children ul")
        this.events() 
    }

    events() {        
        this.menuBtn.on('click', () => this.showChat())
        this.mobileHasChild.on('click', function (){
            $(this).find('ul').slideToggle()
        })
    }

    showChat() {
        this.menu.slideToggle()
        if (this.menuBtn.hasClass("burger-collapse")) {
            this.menuBtn.toggleClass("rise");
            setTimeout(function() {
                $("#burger-collapse-rise").toggleClass("burger-collapse");
            }, 300);
        } else {
            this.menuBtn.toggleClass("burger-collapse");
            setTimeout(function() {
                $("#burger-collapse-rise").toggleClass("rise");
            }, 300);
        }
    }
}

new MobileNav()

