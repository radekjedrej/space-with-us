(function ($) {
    // Navigation hover logic // Most of this logic is to display behind .sub-menu-element // full width bg color
    $('.main-menu__ul li').hover(function (e) {
        const $this = $(this);
        // display block sub-menu on hover
        $this.find('ul').show().addClass('sub-menu');
        // check if element HAVE sub-menu
        if ($this.find('.sub-menu').length !== 0) {			
            // assign height to subNav__wrapper that will give 100% width bg color
            var subMenuHeight = $this.find('.sub-menu').height();
            $('.subNav__wrapper').height(subMenuHeight);
        }
        // check if element NOT HAVE sub-menu
        if ($this.find('.sub-menu').length === 0) {
            // Check if none of the sub menus are visible
            if ($('.sub-menu:visible').length == 0) {
                // remove height from subNav__wrapper
                $('.subNav__wrapper').height(0);
            }
        }
    }, function () {
        // display none sub-menu on hover out
        $(this).find('.sub-menu').hide();
    });

    // Add height from subNav__wrapper - when you enter .sub-menu
    $('.sub-menu').hover(function () {
        var subMenuHeight = $(this).height();
        $('.subNav__wrapper').height(subMenuHeight);
        // Remove height from subNav__wrapper - when you exit .sub-menu
    }, function () {
        if ($(this).closest('menu-item-has-children')) {
            var subMenuHeight = $(this).height();
            $('.subNav__wrapper').height(subMenuHeight);
        } else {
            $('.subNav__wrapper').height(0);
        }
    });

    // Remove height from subNav__wrapper - when you exit .menu-main-navigation
    $('.main-menu__menu').hover(function () {}, function () {
        $('.subNav__wrapper').height(0);
    });


    $(document).on('click', function (e) {
        if ($(e.target).hasClass('alnwick-gardens__action-buttons__book-now')) {
            $('.booking-form__wrapper').toggleClass('is-open');
        } else if (e.target.id != "ot-widget-container5") {
            $('.booking-form__wrapper').removeClass('is-open');
        }

        if ($(e.target).hasClass('alnwick-gardens__action-buttons__plan-event') || $(e.target).parent().hasClass('alnwick-gardens__action-buttons__plan-event')) {
            $('.plan-event-form__wrapper').toggleClass('is-open');
            $(window).scrollTop(0);
        }
    })

})(jQuery);