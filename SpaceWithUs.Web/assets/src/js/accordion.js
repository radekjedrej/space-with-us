class Accordion {
	constructor() {	
        this.accordionButton = $('.js-accordion-toggle')
		this.events()
	}

	events() {		
		this.accordionButton.on('click', function(e) {
            e.preventDefault()
            var $this = $(this);
  
            if ($this.next().hasClass('show-accordion')) {
                $this.next().removeClass('show-accordion');
                $this.removeClass('rotate-accordion');
                $this.next().slideUp(350);
            } else {
                $this.parent().find('li .accordion-fq__inner').removeClass('show-accordion');
                $this.parent().find('.js-accordion-toggle').removeClass('rotate-accordion');      
                $this.parent().find('li .accordion-fq__inner').slideUp(350);
                $this.toggleClass('rotate-accordion');
                $this.next().toggleClass('show-accordion');
                $this.next().slideToggle(350);
            }
        })
	}
}

new Accordion()

