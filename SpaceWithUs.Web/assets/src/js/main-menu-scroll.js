class MobileScroll {
    constructor() {
        this.lastScrollTop = 0
        this.menuHeight = 85
        this.navbar = document.querySelector(".js-space-header")
        this.events() 
    }

    events() {        
        window.addEventListener("scroll", () => this.menuShowOnScroll())
    }

    menuShowOnScroll() {
        this.scrollTop = window.pageYOffset || document.documentElement.scrollTop

        if(this.menuHeight < this.scrollTop ) {
            if (this.scrollTop > this.lastScrollTop) {
                this.navbar.style.top = "-86px"
            } else {
                this.navbar.style.top = "0"
            }
    
            this.lastScrollTop = this.scrollTop
        }
    }
}

new MobileScroll()
