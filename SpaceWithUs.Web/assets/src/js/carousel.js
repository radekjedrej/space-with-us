$('.js-space-carousel').slick({
    centerMode: true,
    centerPadding: '160px',
    slidesToShow: 4,
    responsive: [
        {
        breakpoint: 1480,
        settings: {
            arrows: true,
            centerMode: true,
            centerPadding: '240px',
            slidesToShow: 3
        }
        },  
        {
        breakpoint: 1325,
        settings: {
          arrows: true,
          centerMode: true,
          centerPadding: '80px',
          slidesToShow: 3
        }
        },
        {
        breakpoint: 1025,
        settings: {
          arrows: true,
          centerMode: true,
          centerPadding: '80px',
          slidesToShow: 2
        }
        },
        {
        breakpoint: 700,
        settings: {
          arrows: true,
          centerMode: true,
          centerPadding: '160px',
          slidesToShow: 1
        }
        },
        {
          breakpoint: 620,
          settings: {
            arrows: true,
            centerMode: true,
            centerPadding: '130px',
            slidesToShow: 1
          }
          },
        {
        breakpoint: 500,
        settings: {
            arrows: true,
            centerMode: true,
            centerPadding: '60px',
            slidesToShow: 1
        }
        },
        {
          breakpoint: 360,
          settings: {
              arrows: true,
              centerMode: true,
              centerPadding: '10px',
              slidesToShow: 1
          }
          }
    ]
});


$('.js-three-column-link').slick({
  centerMode: true,
  arrows: false,
  centerPadding: '20px',
  slidesToShow: 1,
  mobileFirst: true,
  responsive: [
        {
          breakpoint: 500,
          settings: {
            centerPadding: '60px',
        }
        },
        {
          breakpoint: 767,
          settings: {
            centerPadding: '170px',
        }
        },
        {
          breakpoint: 1023,
          settings: 'unslick'
        }
  ]
});

$('.js-three-column-static').slick({
  centerMode: true,
  arrows: false,
  centerPadding: '20px',
  slidesToShow: 1,
  mobileFirst: true,
  responsive: [
        {
          breakpoint: 500,
          settings: {
            centerPadding: '60px',
        }
        },
        {
          breakpoint: 767,
          settings: {
            centerPadding: '170px',
        }
        },
        {
          breakpoint: 1023,
          settings: 'unslick'
        }
  ]
});

$('.js-home-billboard').slick({
  slidesToShow: 1,
  nextArrow: '.next-caro',
  prevArrow: '.previous-caro'
});

$('.js-secondary-billboard').slick({
  slidesToShow: 1,
  dots: true,
  nextArrow: '.next-caro-secondary',
  prevArrow: '.previous-caro-secondary'
});